# Freelancer

A Hugo theme to show off contact information, a portrait and a company logo. 
Intended as a way for customers to get in touch with freelancers.

## Installation

```
git submodule add https://gitlab.com/rjh.events/hugo-theme-freelancer.git themes/freelancer
```

## Acknowledgments

Based on Pedro Lopez's tutorial [Creating a Hugo Theme From Scratch](https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/)

This theme uses [Feather Icons](https://feathericons.com/) and [Bootstrap CSS](https://getbootstrap.com/).
